import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import { AppLoading } from "expo";
import { Provider as ReduxProvider, useSelector, useDispatch } from 'react-redux';
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from './src/view/redux/store';
import WelcomeScreen from './screens/WelcomeScreen';
import MapScreen from './screens/MapScreen';
import HomeScreen from './src/view/screens/homeScreen';
import MainScreen from './src/view/screens/mainScreen';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { mapping, light, dark } from "@eva-design/eva";
import { EvaIconsPack } from "@ui-kitten/eva-icons";
import SplashScreen from './src/view/screens/splashScreen';
import SendHeartScreen from './src/view/screens/setting/sendHeartScreen'
import LocationScreen from './src/view/screens/setting/locationScreen'
import PubNub from 'pubnub';
import { PubNubProvider } from "pubnub-react";
import LoginScreen from './src/view/screens/auth/loginScreen';
import SignUpScreen from './src/view/screens/auth/signUpScreen';
import ConnectSuccessScreen from './src/view/screens/map/connect/connectSuccessScreen';
import { navigationName } from './src/global/constants';
import ChangeNameChildrenScreen from './src/view/screens/map/change-name-children/changeNameChildrenScreen';
import ChatScreen from './src/view/screens/chatScreen';
import ChildrenListScreen from './src/view/screens/childrenList/childrenListScreen';

const pubnub = new PubNub({
  publishKey: "pub-c-94adb7c6-a03f-45e7-9b33-1f140f9d53c7",
  subscribeKey: "sub-c-dffd05c8-7b24-11ea-9770-0a12e0cf0d6e",
});
const Stack = createStackNavigator();

function HomeCreator() {
  return (
    <Stack.Navigator screenOptions={styles.defaultNavigationOptions} initialRouteName={navigationName.loginScreen}>
      <Stack.Screen name={navigationName.splashScreen} component={SplashScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.loginScreen} component={LoginScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.signUpScreen} component={SignUpScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.homeScreen} component={HomeScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.welcomeScreen} component={WelcomeScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.childrenListScreen} component={ChildrenListScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.connectSuccessScreen} component={ConnectSuccessScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.changeNameChildrenScreen} component={ChangeNameChildrenScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.chatScreen} component={ChatScreen} options = {{headerShown:true}} />
      <Stack.Screen name={navigationName.mainScreen} component={MainScreen} options={{ headerShown: false }} />
      <Stack.Screen name={navigationName.mapScreen} component={MapScreen} options={{ headerShown: true, title: 'Bản đồ' }} />
      <Stack.Screen name={navigationName.locationScreen} component={LocationScreen} options={{ headerShown: true }} />
      <Stack.Screen name={navigationName.sendHeartScreen} component={SendHeartScreen} options={{ headerShown: true }} />
    </Stack.Navigator>
  );
}

export default function App() {
  const [isReady, setIsReady] = useState(false);


  function handleResourcesAsync() {
    console.log('loading');
  };

  if (!isReady)
    return (
      <AppLoading startAsync={handleResourcesAsync}
        onError={error => console.warn(error)}
        onFinish={() => setIsReady(true)}
      />
    );

  return (
    <ReduxProvider store={store}>
      <PersistGate loading={<AppLoading />} persistor={persistor}>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider mapping={mapping} theme={light}>
          <NavigationContainer>
            <PubNubProvider client={pubnub}>
              <HomeCreator></HomeCreator>
              {/* <ChildrenListScreen/> */}
            </PubNubProvider>
          </NavigationContainer>
        </ApplicationProvider>
      </PersistGate>
    </ReduxProvider>
  )

}

const styles = {
  defaultNavigationOptions: {
    headerStyle: {
      height: 80,
      backgroundColor: "#eee",
      borderBottomColor: "transparent",
      elevation: 0 // for android
    },
  }
}