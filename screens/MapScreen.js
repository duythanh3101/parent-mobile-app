import React, { useState, useEffect, useRef } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, Alert } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MapView, { Marker, Polyline, Callout, PROVIDER_GOOGLE } from 'react-native-maps';
import PubNubReact, { usePubNub } from 'pubnub-react';
import { useSelector, useDispatch } from 'react-redux';
import { addChildren, addLatestChildrenLocation, deleteAllData } from '../src/view/redux/actions/mapDataActions'
import Geocoder from 'react-native-geocoding';

import { YellowBox } from "react-native";
import _ from "lodash";
import { useTheme } from '@react-navigation/native';
import { colors } from '../src/global/styles';
YellowBox.ignoreWarnings(["Setting a timer"]);
const _console = _.clone(console);
console.warn = (message) => {
  if (message.indexOf("Setting a timer") <= -1) {
    _console.warn(message);
  }
};

export default function MapScreen(props) {
  const mapData = useSelector(state => state.mapDataReducer);
  const dispatch = useDispatch();
  const mapRef = useRef(null)
  const markerRef = useRef(null)
  const pubnub = usePubNub();

  //const [existedUser, setExistedUser] = useState(false)
  const parentKey = mapData.parentKey ? mapData.parentKey : '';

  const [state, setState] = useState({
    currentLocation: {
      latitude: -1,
      longitude: -1
    },
    users: new Map(),
    currentChildUUID: '',
    parentKey: parentKey,
    routeCoords: []
  })

  const [coords, setCoords] = useState([]);

  const [initialRegion, setInitialRegion] = useState({
    latitude: 15.7004620,
    longitude: 106.7621181,
    latitudeDelta: 40.0001,
    longitudeDelta: 40.0001
  })

  const [userCount, setUserCount] = useState(0)

  useEffect(() => {
    //console.log('Mapdata ', mapData);
    setUserCount(mapData.totalOccupant)
    if (mapData.totalOccupant > 0) {
      if (mapData.users[userCount - 1] && mapData.users[userCount - 1].uuid && mapData.users[userCount - 1].lastestLocation && mapData.users[userCount - 1].lastestLocation.latitude !== 0 && mapData.users[userCount - 1].lastestLocation.longitude !== 0) {
        //setExistedUser(true);
        console.log('ComponentDidMount ');

        let users = state.users

        const newUser = {
          uuid: mapData.users[userCount - 1].uuid,
          latitude: mapData.users[userCount - 1].lastestLocation.latitude,
          longitude: mapData.users[userCount - 1].lastestLocation.longitude,
        };

        users.set(newUser.uuid, newUser);


        // if (newUser.latitude && newUser.longitude) {
        dispatch(addLatestChildrenLocation(newUser.uuid, {
          latitude: newUser.latitude,
          longitude: newUser.longitude
        }));

        setState({
          currentLocation: {
            latitude: newUser.latitude,
            longitude: newUser.longitude
          },
          users: users,
          currentChildUUID: mapData.users[userCount - 1].uuid,
          routeCoords: [...state.routeCoords, {
            latitude: newUser.latitude,
            longitude: newUser.longitude
          }]
        });
        setCoords([...coords, {
          latitude: newUser.latitude,
          longitude: newUser.longitude
        }])

        focusLocation(newUser.latitude, newUser.longitude)

        // setInitialRegion({
        //   latitude: newUser.latitude,
        //   longitude: newUser.longitude,
        //   latitudeDelta: 0.01,
        //   longitudeDelta: 0.01
        // })
      }

      //}
    }
  }, [])

  useEffect(() => {
    console.log('users: ', state.users)
    const listener = {
      status: function (statusEvent) {
        if (statusEvent.category === "PNConnectedCategory") {
          console.log("Map screen is Connected!")
        }
      },
      message: function (msg) {
        let users = state.users

        let oldUser = state.users.get(msg.publisher);

        const newUser = {
          uuid: msg.message.uuid,
          latitude: msg.message.latitude,
          longitude: msg.message.longitude,
        };

        // if (newUser.latitude && newUser.longitude) {
        dispatch(addLatestChildrenLocation(newUser.uuid, {
          latitude: newUser.latitude,
          longitude: newUser.longitude
        }));

       
        // }


        if (msg.message.message) {
          Timeout.set(msg.publisher, this.clearMessage, 5000, msg.publisher);
          newUser.message = msg.message.message;
        } else if (oldUser) {
          newUser.message = oldUser.message
        }

        updateUserCount();
        users.set(newUser.uuid, newUser);

        setState({
          currentLocation: {
            latitude: newUser.latitude,
            longitude: newUser.longitude
          },
          users: users,
          currentChildUUID: newUser.uuid,
          routeCoords: [...state.routeCoords, {
            latitude: newUser.latitude,
            longitude: newUser.longitude
          }]
        });
        setCoords([...coords, {
          latitude: newUser.latitude,
          longitude: newUser.longitude
        }])
        //console.log('Route coords 2', coords);

        // setInitialRegion({
        //   latitude: newUser.latitude,
        //   longitude: newUser.longitude,
        //   latitudeDelta: 0.01,
        //   longitudeDelta: 0.01
        // })
        focusLocation(newUser.latitude, newUser.longitude)
      }
    }

    pubnub.addListener(listener);

    pubnub.subscribe({
      channels: [`${parentKey}`],
      withPresence: true
    });

    //console.log('coords: ', state.routeCoords);

    return () => {
      pubnub.removeListener(listener);
      pubnub.unsubscribeAll()

      dispatch(addLatestChildrenLocation(state.currentChildUUID, {
        latitude: state.currentLocation.latitude,
        longitude: state.currentLocation.longitude
      }));
    }


  }, [pubnub])


  const focusLoc = () => {
    console.log('Focusloc user: ', state.users)

    let child = state.users.get(state.currentChildUUID);

    if (!child) {
      return;
    }

    let region = {
      latitude: child.latitude,
      longitude: child.longitude,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01
    };
    mapRef.current.animateToRegion(region, 1000);

  }

  const focusLocation = (latitude, longitude) => {

    let region = {
      latitude: latitude,
      longitude: longitude,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01
    };
    mapRef.current.animateToRegion(region, 1000);
  }

  const updateUserCount = () => {
    var presenceUsers = 0;
    pubnub.hereNow({
      includeUUIDs: true,
      includeState: true
    },
      function (status, response) {
        // handle status, response
        presenceUsers = response.totalOccupancy;
      });
    var totalUsers = Math.max(presenceUsers, state.users.size)
    setState({ userCount: totalUsers })
  };

  const animateToCurrent = (coords, speed) => {
    region = {
      latitude: coords.latitude,
      longitude: coords.longitude,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01
    };
    mapRef.current.animateToRegion(region, speed);
  };

  const onRegionChangeComplete = () => {
    if (markerRef && markerRef.current && markerRef.current.showCallout) {
      markerRef.current.showCallout();
    }
  }

  const onHandleMarkerPress = () => {
    var NY = {
      lat: state.currentLocation.latitude,
      lng: state.currentLocation.longitude
    };
    console.log('NY', NY);

    Geocoder.init("AIzaSyBeu7yvq_pdz3r5NVB0t-ffcOqGUAUb3tY"); // use a valid API key


    Geocoder.from(NY.lat, NY.lng)
      .then(json => {
        var addressComponent = json.results[0].address_components[0];
        console.log('address', addressComponent);
      })
      .catch(error => console.warn(error));

    //Alert.alert('Đang cập nhật');

  }

  return (
    <View style={styles.mapContainer} >
      <MapView
        style={styles.map}
        ref={mapRef}
        // onMoveShouldSetResponder={this.draggedMap}
        initialRegion={{
          latitude: 15.7004620,
          longitude: 106.7621181,
          latitudeDelta: 40.0001,
          longitudeDelta: 40.0001
        }}
        onRegionChangeComplete={onRegionChangeComplete}
      >
        {
          (state.users && state.users.size > 0 && state.currentChildUUID) ? (
            <Marker
              style={styles.marker}
              key={state.users.get(state.currentChildUUID).uuid}
              coordinate={{
                latitude: state.users.get(state.currentChildUUID).latitude ? state.users.get(state.currentChildUUID).latitude : 0,
                longitude: state.users.get(state.currentChildUUID).longitude ? state.users.get(state.currentChildUUID).longitude : 0
              }}
              image={require('../assets/image/location.png')}
              ref={markerRef}
              onPress={onHandleMarkerPress}
            >

              <Callout tooltip>
                <View>
                  <View style={styles.buble}>
                    <Text style={{ color: colors.white }}>{mapData.users[userCount - 1].name} was here just now</Text>
                  </View>

                  <View style={styles.arrowBorder} />
                </View>

              </Callout>

            </Marker>
          ) : null
        }
        {/* <Polyline
          coordinates={coords
            // [
            //   { latitude: 10.815826, longitude: 106.6318356 },
            //   { latitude: 10.715826, longitude: 106.6318356 },



            // ]
          }
          strokeWidth={3}
          strokeColor="red" /> */}
      </MapView>

      <View style={styles.topBar}>
        <View style={styles.rightBar}>
          <TouchableOpacity onPress={() => { alert('Khóa của bạn: ' + state.parentKey) }}>
            <Image style={styles.focusLoc} source={require('../assets/image/info.png')} />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.bottom}>
        <View style={styles.bottomRow}>
          <TouchableOpacity onPress={() => focusLoc()}>
            <Image style={[styles.focusLoc, { width: 30, height: 30 }]} source={require('../assets/image/crosshair.png')} />
          </TouchableOpacity>
        </View>
      </View>
    </View >
  );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  mapContainer: {
    flex: 1
  },
  bottomRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  marker: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: Platform.OS === "android" ? 100 : 0,
  },
  topBar: {
    top: Platform.OS === "android" ? hp('2%') : hp('5%'),
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: wp("2%"),
  },
  rightBar: {
    justifyContent: "center",
    alignItems: "flex-end",
    flex: 1
  },
  bottom: {
    position: "absolute",
    flexDirection: 'column',
    bottom: 0,
    justifyContent: "center",
    alignSelf: "center",
    width: "100%",
    marginBottom: hp("10%"),
  },
  focusLoc: {
    width: hp("4.5%"),
    height: hp("4.5%"),
    marginRight: wp("2%"),
    left: 15
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  profile: {
    width: hp("10.5%"),
    height: hp("10.5%")
  },

  buble: {
    backgroundColor: colors.blue,
    padding: 5,
    borderRadius: 10,
    alignItems: 'center',
    width: 'auto'
  },
  arrow: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: colors.blue,
    borderWidth: 14,
    alignSelf: 'center',
    marginTop: -34,
  },
  arrowBorder: {
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderTopColor: colors.blue,
    borderWidth: 14,
    alignSelf: 'center',
  },
  image: {
    width: hp("10.5%"),
    height: hp("10.5%")
  }
});