import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, ImageBackground, TouchableOpacity, BackHandler } from 'react-native';
import { usePubNub } from "pubnub-react";
import { globalStyles, images, colors } from '../src/global/styles';
import { Ionicons, AntDesign } from '@expo/vector-icons';
import { useSelector, useDispatch } from 'react-redux';
import { addChildren, addParentKey, deleteAllData } from '../src/view/redux/actions/mapDataActions'
import { navigationName } from '../src/global/constants';
import { generateRandomString } from '../src/view/Utils/utilities';
import mapService from '../src/core/services/map/mapService';

export default function WelcomeScreen(props) {

  const mapData = useSelector(state => state.mapDataReducer);
  const dispatch = useDispatch();
  const pubnub = usePubNub();

  const [state, setState] = useState({
    parentKey: '',
  })

  useEffect(() => {
    let parentKey = generateRandomString(3);
    //let parentKey = 'ABCDA';
    setState({ parentKey });

    dispatch(addParentKey(parentKey));

    const listener = {
      message: function (msg) {
        //console.log("Nhan duoc message:", msg.message)
        const msgKeyNormalize = msg.message.key ? msg.message.key.normalize() : '';
        const parentKeyNormalize = parentKey.normalize();
        //console.log("so sanh:", msgKeyNormalize === parentKeyNormalize, parentKeyNormalize, msgKeyNormalize)

        const childrenKey = msg.message.uuid;
        if (msgKeyNormalize === parentKeyNormalize) {
          pubnub.publish({
            message: {
              success: true
            },
            channel: `${parentKey}-connect-${childrenKey}`
          });
          console.log('Ket noi thanh cong: ' + childrenKey);

          dispatch(addChildren(
            childrenKey, '',
            {
              latitude: 0,
              longitude: 0
            }
          ));
          mapService.addChildren(uuid, 'thanh 1');
          props.navigation.navigate(navigationName.connectSuccessScreen);
        }
      },
      status: function (statusEvent) {
        if (statusEvent.category === "PNConnectedCategory") {
          console.log("Connected to PubNub!")
        } else {
          console.log("Connected to failed!")
        }
      },
    }

    pubnub.addListener(listener);
    pubnub.subscribe({
      channels: [`${parentKey}-connect`],
      withPresence: true,
    });
    console.log('subcribe success:', parentKey);


    return () => {
      pubnub.removeListener(listener);
      pubnub.unsubscribeAll()
    }

  }, [pubnub]);

  const onHandleCancelPress = () => {
    BackHandler.exitApp();
  }


  return (
    <View style={globalStyles.container}>
      <ImageBackground source={images.backgroundHomeScreen} style={styles.image}>
        <View style={styles.wrapper}>
          <Text style={[globalStyles.normalCenterText, styles.header]}>To see your child on the map install the Chat with Parents app on the child's phone</Text>


          <View style={styles.iconChat}>
            <Ionicons name="ios-chatboxes" size={60} color="white" />
          </View>

          <Text style={[globalStyles.normalCenterText, styles.header]}> and enter this code </Text>
          <Text style={[globalStyles.normalCenterText, styles.textCode]}> {state.parentKey} </Text>
          <View style={styles.codeExpireContainer}>
            <AntDesign name="exclamationcircleo" size={18} color="black" />
            <Text style={globalStyles.normalCenterText}> Days until code expires: 3</Text>
          </View>

        </View>

        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.buttonCancel} onPress={onHandleCancelPress}>
            <Text style={styles.cancelText}>Cancel</Text>
          </TouchableOpacity>
        </View>

      </ImageBackground>

    </View>
  )
}

const styles = StyleSheet.create({
  backgroundHomeScreen: {
    flexDirection: 'column',
  },
  cancelText: {
    fontSize: 24
  },
  buttonCancel: {

  },
  buttonContainer: {
    justifyContent: 'flex-end',
    height: 40,
    width: 200,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    alignSelf: 'center',

  },
  codeExpireContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  iconChat: {
    width: 100,
    height: 100,
    borderRadius: 200,
    backgroundColor: colors.lightblue,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
  wrapper: {
    backgroundColor: '#FFFFFF',
    margin: 10,
    marginTop: 40,
    height: '45%',
    borderRadius: 15,
    alignItems: 'center',
  },
  header: {
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold',
    marginTop: 10,
    padding: 10
  },
  textCode: {
    color: colors.lightblue,
    fontSize: 36,
    fontWeight: 'bold'
  }
});