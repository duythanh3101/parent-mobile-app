
export const rootEndpoint = "http://child-safety-api.herokuapp.com/api";
export const authSignUpEndpoint = rootEndpoint + "/auth/signup";
export const authSignInEndpoint = rootEndpoint + "/auth/signin";
export const locationEndpoint = rootEndpoint + "/location";
export const addChildrenEndpoint = rootEndpoint + "/parents/child";
export const sendChildLocation5minsEndpoint = rootEndpoint + "/parents/child";
export const getChildrenListEndpoint = rootEndpoint + "/parents/child";
export const getAllInfoParentEndpoint = rootEndpoint + "/parents/me";