import { Texts } from '../../texts';
import { authSignUpEndpoint } from '../../apis/endpoints';
import BaseService from '../baseService';
import { validatePassword, validateUsername } from '../validation';

class SignUpService extends BaseService {
    constructor(){
        super(authSignUpEndpoint)
    }
 
    async signup(username, password) {
        return await this.createNewAccount(username, password);
    }

    async createNewAccount(username, password) {
        // const checkUsername = await this.isValidUsername(username);
        // if (checkUsername.error)
        //     return checkUsername;

        // if (!validatePassword(password))
        //     return { error: Texts.INVALID_PASSWORD };

        return await this.create({ username, password });
    }

    /**
     * @param {string} username 
     */
    async isValidUsername(username) {
        if (!validateUsername(username))
            return { error: Texts.INVALID_USERNAME };

        const existAccount = await this.query({ username: username });
        if (existAccount.data.length > 0)
            return { error: Texts.USERNAME_ALREADY_EXISTS };

        return { username: username }
    }
}

export default new SignUpService();