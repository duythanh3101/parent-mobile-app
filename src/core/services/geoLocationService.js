import { useSelector } from "react-redux";

class GeoLocationService {
  constructor(props) {
    super(props);

    this.pubnub = new PubNubReact({
      publishKey: "pub-c-94adb7c6-a03f-45e7-9b33-1f140f9d53c7",
      subscribeKey: "sub-c-dffd05c8-7b24-11ea-9770-0a12e0cf0d6e"
    });

    const mapData = useSelector(state => state.mapDataReducer)

    //Base State
    this.state = {
      channelName: `${mapData.parentKey}-connect`,
    }

    this.pubnub.init(this);
  }


  getMessage = () => {
    
    if (this.state.channelName)
    {
      return this.pubnub.getMessage(`${this.state.channelName}`);
    }
    return null;
  }

  subcribe = (channel = '', isPresence = true) => {

    if (channel) {
      this.setState({ channelName: channel });

      this.pubnub.subscribe({
        channels: [{ channel }],
        withPresence: isPresence
      });
    }

  };

  publish = (msg, channel) => {
    this.pubnub.publish({
      message: msg,
      channel: channel
      }
    )
  }

}

export default new GeoLocationService();
