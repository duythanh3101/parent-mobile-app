import { addChildrenEndpoint } from '../../apis/endpoints';
import BaseService from '../baseService';

class MapService extends BaseService {
    constructor(){
        super(addChildrenEndpoint)
    }
 
    async addChildren(key, name) {
        return await this.create({ key, name });
    }

    async getAllInfoParent(){
        return await this.getAll();
    }
}

export default new MapService();


