import { Dimensions } from "react-native";

export const constants = {
    screenWidth: Math.round(Dimensions.get('window').width),
    screenHeight: Math.round(Dimensions.get('window').height),

}

export const navigationName = {
    splashScreen: 'SplashScreen',
    loginScreen: 'LoginScreen',
    signUpScreen: 'SignUpScreen',
    homeScreen: 'HomeScreen',
    welcomeScreen: 'WelcomeScreen',
    connectSuccessScreen: 'ConnectSuccessScreen',
    changeNameChildrenScreen: 'ChangeNameChildrenScreen',
    mainScreen: 'MainScreen',
    childrenListScreen: 'ChildrenListScreen',
    mapScreen: 'MapScreen',
    locationScreen: 'LocationScreen',
    sendHeartScreen: 'SendHeartScreen',
    chatScreen:"ChatScreen"
}