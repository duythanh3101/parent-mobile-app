import { StyleSheet } from 'react-native'

export const images = {
    backgroundHomeScreen: require('../../assets/image/background-map.jpg'),
    microIcon: require('../../assets/image/micro-icon.png'),
    listenerIcon:require('../../assets/image/listener-icon.png'),
    locationImage:require('../../assets/image/location.png'),
}

export const colors = {
    mainColor: '#FFFFFF',
    accent: "#2196f3",
    primary: "#0AC4BA",
    secondary: "#2BDA8E",
    tertiary: "#FFE358",
    black: "#2F2F2F",
    white: "#FFFFFF",
    blue: '#2196f3',
    lightblue: '#1A9EE8',
    gray: "#2B2C30",
    darkgray: '#181B20',
    lightGreen: '#49CC90',
    
  };

export const globalStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.mainColor

    },
    titleText: {
        fontSize: 18,
        color: colors.black,
        fontWeight: 'bold',
    },
    headerText: {
        fontSize: 24,
        fontWeight: 'bold',
        color: colors.black,
    },
    headerCenterText: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: "center",
        justifyContent: 'center',
        color: colors.black,
    },
  
    normalCenterText: {
        color: colors.black,
        textAlign: "center",
        fontSize: 14,
    },
    normalText: {
        color: colors.black,
        fontSize: 14,
    },


    paragraph: {
        lineHeight: 20,
        marginVertical: 8,
    },
    input: {
        borderWidth: 2,
        borderColor: '#ddd',
        padding: 10,
        fontSize: 18,
        borderRadius: 6,
        width: '80%'
    },
    buttonStyle: {
        backgroundColor: 'red',
        width: 100,
    },
    errorText: {
        color: 'crimson',
        fontWeight: 'bold',
        marginTop: 6,
        marginBottom: 10,
    },
    modalContent: {
        flex: 1,
    },
    modalContainer: {
        height: '30%',
        padding: 20,
        flex: 1

    }
});