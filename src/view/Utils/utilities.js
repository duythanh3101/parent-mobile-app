


export function generateRandomString(length){
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";//0123456789";

    for (var i = 0; i < length; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
  }