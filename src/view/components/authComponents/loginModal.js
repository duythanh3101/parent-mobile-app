import React, { useState} from 'react'
import { View, Text, Modal, TouchableWithoutFeedback, Keyboard, StyleSheet, TextInput, Button, CheckBox } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import { globalStyles } from '../../../global/styles'
import { Input } from 'react-native-elements'

export default function LoginModal() {
    const [modalVisible, setModalVisible] = useState(false)

    return (
        <View style={globalStyles.modalContainer}>
            <Modal visible={modalVisible} animationType='slide'>
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <View style={styles.modalContent}>
                    <MaterialIcons
                                name='close'
                                size={24}
                                onPress={() => setModalVisible(false)}
                                style={{...styles.modalToggle, ...styles.modalClose}}
                                />
                        <View style={styles.headerContainer}>
                            <Text style={globalStyles.headerText}>Accept the terms</Text>
                            <Text style={globalStyles.headerText}> of the Service Agreement</Text>
                        </View>
                        <View style={styles.headerContainer}>
                            <Text style={globalStyles.titleText}>Enter your email</Text>
                        </View>
                        <TextInput 
                            style={globalStyles.input}
                            placeholder='Email'
                            onChangeText={() => {}}
                            />
                       <View style={styles.headerContainer}>
                            <Text style={styles.normalText}>Your email is necessary to log into the web version of the Find my Kid's app or to re-login the application</Text>
                        </View>

                        <View>
                            <View style={{flexDirection: 'row', alignItems:'center',justifyContent:'center'}}>
                                <CheckBox style={{borderWidth: 10, borderColor: 'red', size: 30, margin: 10}}/>
                                <Text style={globalStyles.normalText, {alignSelf:'center',textAlign:'center'}}>I accept the Terms of Use and Privacy Policy</Text>

                            </View>


                        </View>
                        <Button title='next' style={{width: '100%'}}/>


                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </View>
    )
}


const styles = StyleSheet.create({
    modalToggle: {
        marginBottom: 10,
        borderWidth: 1,
        borderColor: 'red',
        padding: 10,
        borderRadius: 10,
        alignSelf: 'center'
    },
    modalClose: {
        marginTop: 20,
        marginBottom: 0
    },
    modalContent: {
        flex: 1,
        alignItems: 'center',
        height: '30%'
    },
    headerContainer: {
        margin: 15
    },
    normalText: {
        fontSize: 18,
        marginLeft: '5%',
        marginRight: '5%',
        color: 'gray',
        fontWeight: 'bold'
    }
})