import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { images } from '../../../global/styles'

const ChildrenListItem = (props) => {
    return (
        <TouchableOpacity style={styles.cardContainer}>
            <View style={styles.cardContent}>
                <Image source={images.locationImage} style={styles.avatar} />
                <Text style={styles.nameText}>{props.name}</Text>
            </View>

        </TouchableOpacity>

    )
}

export default ChildrenListItem

const styles = StyleSheet.create({
    nameText: {
        fontSize: 24,
        margin: 10
    },
    cardContainer: {
        elevation: 3,
        backgroundColor: '#FFF',
        marginVertical: 4,
        marginHorizontal: 10,
        shadowColor: '#333'
    },
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 40,
        marginLeft: 10
    },
    cardContent: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    }
})
