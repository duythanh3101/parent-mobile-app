import { combineReducers } from "redux";
import authReducer from "./authReducer";
import mapDataReducer from './mapDataReducer';

export default combineReducers({ authReducer, mapDataReducer });