import { globalActionTypes } from '../actions/actionTypes'

const initialState = {
    /**
     * Count of connected users
     */
    totalOccupant: 0,
    /**
     * Store data of all connected users
     */
    users: [],
    /** 
     * Key 
     */
    parentKey: '',
    /**
     * expiration date
     */
    expirationDate: 7
};

export default function (state = initialState, action) {
    switch(action.type) {
        case globalActionTypes.MAPDATA_ADD_CHILDREN: {
            const user = {
                uuid: action.payload.uuid,
                name: action.payload.name,
                lastestLocation: action.payload.lastestLocation
            }
            return {
                ...state,
                totalOccupant: state.totalOccupant + 1,
                users: [...state.users, user],
            }
        }
        case globalActionTypes.MAPDATA_ADD_PARENTKEY: {
            const { parentKey } = action.payload;
            return {
                ...state,
                parentKey: parentKey
            }
        }
        case globalActionTypes.MAPDATA_ADD_NICKNAME_CHILDREN: {
            const { uuid, nickname } = action.payload;
            return {
                ...state,
                users: state.users.map((child, i) => child.uuid === uuid ? { ...child, name: nickname}: child)
            }
        }
        case globalActionTypes.MAPDATA_ADD_LASTEST_CHILDREN_LOCATION: {
            const { uuid, location } = action.payload;
            return {
                ...state,
                users: state.users.map((child, i) => child.uuid === uuid ? { ...child, lastestLocation: {
                    latitude: location.latitude,
                    longitude: location.longitude
                }}: child)
            }
        }
        case globalActionTypes.MAPDATA_DELETE_ALL_DATA: {
            return {
                ...state,
                users: [],
                totalOccupant: 0,
                parentKey: ''
            }
        }
        
        default: return state;
    }
};