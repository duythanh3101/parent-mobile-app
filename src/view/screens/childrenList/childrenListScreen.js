import React from 'react'
import { StyleSheet, Text, View, Image, SafeAreaView } from 'react-native'
import { globalStyles, images } from '../../../global/styles'
import ChildrenListItem from '../../components/childrenList/childrenListItem'
import { navigationName } from '../../../global/constants'
import { useSelector, useDispatch } from 'react-redux'
import { FlatList } from 'react-native-gesture-handler'

const ChildrenListScreen = (props) => {

    const mapData = useSelector(state => state.mapDataReducer);

    const renderItem = (item, index) => {

        if (item.name){
            return (
                <ChildrenListItem
                    name={item.name}
                    onPress={onHandlePress(item.uuid)}
                />
            )
        }
        else{
            return (
                <ChildrenListItem
                    name="default"
                />
            )
        }
        
    }

    const onHandlePress = (uuid) => {
        props.navigation.navigate(navigationName.mapScreen, {
            uuid: uuid
        })
    }

    return (
        <SafeAreaView>
            <View styles={globalStyles.container}>
                <Text style={globalStyles.headerCenterText}>Children List</Text>
                
                <FlatList
                    data={mapData.users}
                    renderItem={renderItem}
                />

                <ChildrenListItem name="thanh" onPress={onHandlePress} />
               

            </View>
        </SafeAreaView>


    )
}

export default ChildrenListScreen

const styles = StyleSheet.create({
    nameText: {
        fontSize: 24,
        margin: 10
    },
    cardContainer: {
        elevation: 3,
        backgroundColor: '#FFF',
        marginVertical: 4,
        marginHorizontal: 10,
        shadowColor: '#333'
    },
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 40,
        marginLeft: 10
    },
    cardContent: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    }
})
