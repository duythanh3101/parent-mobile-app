

import React, { useEffect, useState } from 'react'
import { Text, View, ImageBackground, StyleSheet } from 'react-native';
import { images } from '../../global/styles';
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import LoginModal from '../components/authComponents/loginModal';
import { useSelector, useDispatch } from 'react-redux';
import { login, logout } from '../redux/actions/authActions';
import { AppLoading } from 'expo';

export default function HomeScreen({ navigation }) {
  const [isReady, setIsReady] = useState(false);

  const handlePressPhone = () => {
    navigation.navigate('Welcome');
  }

  const handlePressGPSWatch = () => {
    console.log('hihi');
  }

  const handlePressNotHaveGPSWatch = () => {
    console.log('hihi');
  }


  useEffect(() => {
    setIsReady(true);
  }, [])

  if (!isReady)
    return <AppLoading/>

  return (
    <View style={styles.container}>
      <ImageBackground source={images.backgroundHomeScreen} style={styles.image}>
        <View style={styles.wrapper}>
          <Text style={styles.header}>Connect your child's device to see his/her location on the map</Text>

          <View style={styles.allLines}>
            <TouchableOpacity style={styles.lineItem} onPress={handlePressPhone}>
              <MaterialCommunityIcons name="cellphone" size={32} color="blue" />
              <Text>Connect a kid's phone</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.lineItem} onPress={handlePressGPSWatch}>
              <MaterialCommunityIcons name="watch-variant" size={32} color="blue" />
              <Text>Connect a GPS-watch</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.lineItem} onPress={handlePressNotHaveGPSWatch}>
              <MaterialIcons name="face" size={32} color="blue" />
              <Text>The child doesn't have GPS-watch and phone</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <LoginModal /> */}
      </ImageBackground>

    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  image: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
  wrapper: {
    backgroundColor: '#FFFFFF',
    margin: 10,
    marginTop: 40,
    height: '30%',
    borderRadius: 15,
  },
  header: {
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold',
    marginTop: 10
  },
  lineItem: {
    flexDirection: 'row',
    margin: 10,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  allLines: {
    marginTop: 20
  }
});