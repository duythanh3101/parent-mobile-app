import React, {useState} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, TextInput } from 'react-native'
import { globalStyles, colors } from '../../../../global/styles'
import { navigationName } from '../../../../global/constants'
import { useDispatch, useSelector } from 'react-redux'
import { addNickNameChildren } from '../../../redux/actions/mapDataActions'

const ChangeNameChildrenScreen = (props) => {

    const [text, setText] = useState('');
    const mapData = useSelector(state => state.mapDataReducer);
    const dispatch = useDispatch();

    const onHandleGonext = () => {
        if (text){
            dispatch(addNickNameChildren(mapData.users[mapData.totalOccupant - 1].uuid, text));
            props.navigation.navigate(navigationName.mainScreen);
        }
    }

    const onChangeText = (val) => {
        setText(val);
    }

    return (
        <View style={[globalStyles.container, styles.container]}>
            <View style={styles.textContainer}>
                <Text style={[globalStyles.normalCenterText, styles.textConnected]}>Child's nickname</Text>
                <Text style={[globalStyles.titleText, styles.textPickingName]}>Select the child's nickname that will be displayed in application</Text>

            </View>

            <View style={styles.textInputContainer}> 
                <TextInput placeholder='Select the nickname' onChangeText={onChangeText} style={styles.textInputNickname}/>
            </View>
            <View style={styles.buttonContainer}>
                <TouchableOpacity onPress={onHandleGonext}>
                    <Text style={styles.buttonTextPickName}>Continue</Text>
                </TouchableOpacity>
            </View>



        </View>
    )
}

export default ChangeNameChildrenScreen

const styles = StyleSheet.create({
    textInputContainer: {
        flex: 1,
        alignSelf: 'center'
    },  
    textInputNickname: {
        borderColor: colors.lightblue,
        fontSize: 24,
        borderWidth: 2,
        borderRadius: 10,
        padding: 15,
        paddingLeft: 30,
        paddingRight: 30
    },
    textContainer: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 50
    },
    doItLaterContainer: {
        alignSelf: 'center',
        padding: 20,
        borderRadius: 20,
        justifyContent: 'flex-end',
        marginTop: 20
    },
    buttonTextDoItLater: {
        fontStyle: 'italic',
        textDecorationLine: 'underline',
        fontWeight: 'bold',
        color: colors.blue
    },
    buttonTextPickName: {
        color: '#49CC90',
        fontWeight: 'bold'
    },
    buttonContainer: {
        alignSelf: 'center',
        borderWidth: 1,
        padding: 20,
        paddingLeft: 40,
        paddingRight: 40,
        borderColor: '#49CC90',
        borderRadius: 20,
        justifyContent: 'flex-end',
        marginBottom: 30
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignContent: 'center',
        flexDirection: 'column',
    },
    checkContainer: {
        alignSelf: 'center',
        marginTop: 50
    },
    textConnected: {
        marginTop: 30
    },
    textPickingName: {
        textAlign: 'center',
        marginTop: 50,
        margin: 20
    }

})
