import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { globalStyles, colors } from '../../../../global/styles'
import { Entypo } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { navigationName } from '../../../../global/constants';

const ConnectSuccessScreen = (props) => {

    const onHandleGonext = () => {
        props.navigation.navigate(navigationName.changeNameChildrenScreen);
    }

    const onHandleDoItLater = () => {
        props.navigation.navigate(navigationName.mainScreen);
    }

    return (
        <View style={[globalStyles.container, styles.container]}>
            <View style={styles.checkContainer}>
                <Entypo name="check" size={200} color={colors.lightGreen} />

            </View>
            <Text style={[globalStyles.normalCenterText, styles.textConnected]}>Your child's phone is now connected!</Text>
            <Text style={[globalStyles.titleText, styles.textPickingName]}>Personalize the app by picking a nickname and avatar for your child</Text>

            <View style={styles.groupButtonContainer}>
                <View style={styles.doItLaterContainer}>
                    <TouchableOpacity onPress={onHandleDoItLater}>
                        <Text style={styles.buttonTextDoItLater}>I will do it later</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.buttonContainer}>
                    <TouchableOpacity onPress={onHandleGonext}>
                        <Text style={styles.buttonTextPickName}>Pick a nickname for children</Text>
                    </TouchableOpacity>
                </View>
            </View>

        </View>
    )
}

export default ConnectSuccessScreen

const styles = StyleSheet.create({
    groupButtonContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 50
    },
    doItLaterContainer: {
        alignSelf: 'center',
        padding: 20,
        borderRadius: 20,
        justifyContent: 'flex-end',
        marginTop: 20
    },
    buttonTextDoItLater: {
        fontStyle: 'italic',
        textDecorationLine: 'underline',
        fontWeight: 'bold',
        color: colors.blue
    },
    buttonTextPickName: {
        color: colors.lightGreen,
        fontWeight: 'bold'
    },
    buttonContainer: {
        alignSelf: 'center',
        borderWidth: 1,
        padding: 20,
        borderColor: colors.lightGreen,
        borderRadius: 20,
        justifyContent: 'flex-end',
        marginTop: 20
    },
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignContent: 'center',
        flexDirection: 'column',
    },
    checkContainer: {
        alignSelf: 'center',
        marginTop: 50
    },
    textConnected: {
        marginTop: 30
    },
    textPickingName: {
        textAlign: 'center',
        marginTop: 50,
        margin: 20
    }

})
