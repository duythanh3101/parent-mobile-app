import React from 'react';
import { View, Text } from 'react-native';
import { globalStyles } from '../../../global/styles';
import { useSelector, useDispatch } from 'react-redux';

export default function HistoryTrackingScreen(props) {
    const mapData = useSelector(state => state.mapDataReducer);
    const dispatch = useDispatch();
    console.log('HistoryTrackingScreen: ', mapData.users);

    return (
        <View style={globalStyles.container}>
            <Text>History tracking screen</Text>
            {/* <Text>{mapData.parentKey}</Text>
            <Text>{mapData.totalOccupant > 0 ? mapData.users[0].uuid : 'khong co'}</Text> */}
        </View>
    )
}

