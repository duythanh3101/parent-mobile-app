import React from 'react'
import { View } from 'react-native'
import { Layout, List, ListItem, Icon, Divider } from '@ui-kitten/components';
import { navigationName } from '../../../global/constants';
import { logout } from '../../redux/actions/authActions';
import { useDispatch } from 'react-redux';
import { deleteAllData } from '../../redux/actions/mapDataActions'


export default function SettingScreen({ navigation }) {
    const dispatch = useDispatch();

    const managementFunctionDatas = [
        {
            title: "Location",
            icon: "pin-outline",
            fill: 'green',
            callback: () => navigation.navigate("Location")
        },
        {
            title: "Chat",
            icon: "message-circle-outline",
            fill: 'blue',
            callback: () => { alert("Đang cập nhật") }
        },
        {
            title: "Send hearts",
            icon: "heart-outline",
            fill: 'red',
            callback: () => navigation.navigate("SendHeart")
        },
        {
            title: "Signal",
            icon: "radio-button-on-outline",
            fill: 'red',
            callback: () => { alert("Đang cập nhật") }
        },
        {
            title: "Application Statistics",
            icon: "npm-outline",
            fill: 'blue',
            callback: () => { alert("Đang cập nhật") }
        },
        {
            title: "Add parent or child",
            icon: "people-outline",
            fill: 'red',
            callback: () => { alert("Đang cập nhật") }
        },
        {
            title: "Contact support",
            icon: "phone-call-outline",
            fill: 'gray',
            callback: () => { alert("Đang cập nhật") }
        },
        {
            title: "My account",
            icon: "person-outline",
            fill: 'gray',
            callback: () => { alert("Đang cập nhật") }
        },
        {
            title: "Frequenly asked questions",
            icon: "question-mark-circle-outline",
            fill: 'gray',
            callback: () => { alert("Đang cập nhật") }
        },
        {
            title: "Logout",
            icon: "info-outline",
            fill: 'gray',
            callback: () => { 
                dispatch(logout());
                dispatch(deleteAllData());
                navigation.replace(navigationName.loginScreen);
             }
        },
    ];

    const renderItem = ({ item }) => {
        return (
            <View style={{ marginTop: 10, marginBottom: 10, alignContent: 'center' }}>
                <ListItem
                    title={item.title}
                    icon={style => <Icon {...style} fill={item.fill} name={item.icon} />}
                    accessory={style => <Icon {...style} name="chevron-right-outline" />}
                    onPress={item.callback}
                />
            </View>
        )
    }

    return (
        <View style={{ flex: 1, marginTop: 30 }}>
            <List
                style={{ backgroundColor: 'white' }}
                data={managementFunctionDatas}
                ItemSeparatorComponent={Divider}
                renderItem={renderItem}
            />

        </View>
    )
}

