import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { globalStyles, colors } from '../../global/styles';
import { useSelector, useDispatch } from 'react-redux';
import { login, logout } from '../redux/actions/authActions';
import { navigationName } from '../../global/constants';
import { deleteAllData, addParentKey } from '../redux/actions/mapDataActions';
import { generateRandomString } from '../Utils/utilities';

export default function SplashScreen({ navigation }) {

    const auth = useSelector(state => state.authReducer);
    const mapData = useSelector(state => state.mapDataReducer);
    const dispatch = useDispatch();

    useEffect(() => {
        loadAccount();
    }, []);

    async function loadAccount() {
        try {
            //dispatch(logout());
            //dispatch(login('aa', true));
            //dispatch(deleteAllData())

            if (auth.loggedIn == true) {
                if (mapData.totalOccupant > 1) {
                    setTimeout(() => {
                        navigation.replace(navigationName.childrenListScreen);

                    }, 1000);
                } else if (mapData.totalOccupant > 0){
                    setTimeout(() => {
                        navigation.replace(navigationName.mainScreen);

                    }, 1000);
                } else {
                    setTimeout(() => {
                        navigation.replace(navigationName.welcomeScreen);

                    }, 100);
                }
                
                console.log('da dang nhap');

            } else {
                setTimeout(() => {
                    navigation.navigate(navigationName.loginScreen)

                    console.log('chua dang nhap');
                }, 1000);
            }
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <View style={{ ...globalStyles.container, ...styles.viewStyles }}>
            <Text style={styles.textStyles}>
                Splash Screen
              </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    textStyles: {
        color: 'white',
        fontSize: 40,
        fontWeight: 'bold'
    },

    viewStyles: {
        backgroundColor: colors.blue,
        alignItems: 'center',
        justifyContent: 'center'
    }
})